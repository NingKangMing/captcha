package com.anji.captcha.demo.pojo;

import java.io.Serializable;

/**
 *  使用实体类接收，前端以json字符串的形式传递参数，避免参数接收错误（实测出现+号变空格的情况）
 * @author kangming.ning
 * @date 2023-12-07 17:38
 * @since 1.0
 **/
public class CaptchaParam implements Serializable {

    private String captchaVerification;

    public String getCaptchaVerification() {
        return captchaVerification;
    }

    public void setCaptchaVerification(String captchaVerification) {
        this.captchaVerification = captchaVerification;
    }
}
